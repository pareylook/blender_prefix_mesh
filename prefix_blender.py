bl_info = { 
    "name": "Make Prefix Panel",
    "author": "Evgeny Starostin",
    "category": "3D View"
}

import bpy

def set_pref():
    prf = bpy.context.scene.pref
    sprt = "_"
    obs = bpy.context.selected_objects

    for o in obs:
        o.name = prf + sprt + o.name

def clean_pref():
    obs = bpy.context.selected_objects

    for o in obs:
        long_name = o.name
        clean = long_name.split('_')
        o.name = clean[-1]

class setPrefix(bpy.types.Operator):        
    bl_idname = "set.pref"
    bl_label = "Set_prefix" 
    
    def execute(self, context):
        set_pref()
        return {'FINISHED'}

class cleanPrefix(bpy.types.Operator):        
    bl_idname = "clean.pref"
    bl_label = "Clean_prefix" 
    
    def execute(self, context):
        clean_pref()
        return {'FINISHED'}

class MakePrefixPanel(bpy.types.Panel):
    bl_label = "Make Prefix Panel"
    bl_idname = "OBJECT_PT_hello"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = 'Tools'

    def draw(self, context):
        layout = self.layout

        obj = context.object

        row = layout.row()
        row.prop(context.scene,"pref")
        layout.operator('set.pref')
        layout.operator('clean.pref')



def register():
    bpy.types.Scene.pref = bpy.props.StringProperty(name="New name", default='Your prefix')
    bpy.utils.register_module(__name__)



def unregister():
    bpy.types.Scene.pref = bpy.props.StringProperty(name="New name", default='Your prefix')
    bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()

